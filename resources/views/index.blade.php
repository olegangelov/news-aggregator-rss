<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>News feeds</title>

    <link href="/css/app.css" rel="stylesheet">
    <script src="/js/app.js"></script>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-12 mt-3 mb-3 text-center">
                <a href="#">
                    <h1>News feeds</h1>
                </a>
                <h4 class="text-muted" id="filter-by" value="All content"></h4>
            </div>
        </div>
        <div class="row">
            <div class="col-12 mt-3 mb-3">
                <div id="primary-content"></div>
            </div>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary">More info</button>
                    <button type="button" class="btn btn-secondary">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>