require('./bootstrap');

import Feed from './feed';
import $ from 'jquery';
window.$ = window.jQuery = $;

import 'jquery-ui/ui/widgets/datepicker.js';

$(function () {
    Feed.init();
});