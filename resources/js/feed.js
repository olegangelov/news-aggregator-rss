export class Feed {

    init() {
        Feed.loadData();
    }

    static loadData(param = "", value = "") {

        var api = "/api/get_posts/";

        // Remove any existing entries from FE
        $("#primary-content").html("");
        var filter = "";

        if (param && value) {

            // Set filter name subtitle & api params
            filter = param + "/" + value;
            $("#filter-by").text(value);
        } else {

            // Set default subtitle
            $("#filter-by").text($("#filter-by").attr("value"));
        }

        $.ajax({
            url: api + filter,
            success: function (posts) {
                var loadedPosts = posts;

                // Populate div container with posts returned by API
                posts.forEach(function (post) {

                    var provider = post.provider;
                    var category = post.category;

                    var html = `<a href='#' data-modal='true' data-value="` + post.id + `"><h2>` + post.title + `</h2></a>` +
                        `<p class='mb-5'>by : <a href='#' data-param='provider' data-value="` + provider + `">` + provider + `</a>`;
                    if (post.category) {
                        html += `<span>; category : <a href='#' data-param='category' data-value="` + category + `" >` + category + `</a></span>`;
                    }
                    html += `</p>`;

                    $("#primary-content").prepend(html);
                });

                // Bind element functions
                Feed.processData(loadedPosts);

            }
        });
    }

    static processData(loadedPosts) {

        $("a").off();

        $("a").on("click", function (e) {

            // If link is modal
            if ($(this).attr('data-modal')) {

                var value = $(this).attr("data-value");
                var post = loadedPosts.find(key => key.id == value);

                $(".modal-title").html(post.title);
                $(".modal-body").html(post.description);
                $(".modal").modal("show");

                $(".modal .btn").off();

                $(".modal .btn-secondary").on("click", function () {
                    $(".modal").modal("hide");
                });

                $(".modal .btn-primary").on("click", function () {
                    window.open(post.link, '_blank');
                });

                e.preventDefault();
            } else {

                // Non-modal link sends ajax request to load posts with specific params
                Feed.loadData($(this).attr("data-param"), $(this).attr("data-value"));
            }
        });
    }
}

export default new Feed();
