<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Feed
 * 
 * @property int $id
 * @property string $provider
 * @property string $title
 * @property string|null $link
 * @property string|null $creator
 * @property string|null $description
 * @property string|null $category
 * @property Carbon|null $publication
 *
 * @package App\Models
 */
class Feed extends Model
{
	protected $table = 'feed';
	public $timestamps = false;

	protected $dates = [
		'publication'
	];

	protected $fillable = [
		'provider',
		'title',
		'link',
		'creator',
		'description',
		'category',
		'publication'
	];
}
