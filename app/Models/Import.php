<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Import
 * 
 * @property int $id
 * @property string $resource
 * @property Carbon $date
 *
 * @package App\Models
 */
class Import extends Model
{
	protected $table = 'import';
	public $timestamps = false;

	protected $dates = [
		'date'
	];

	protected $fillable = [
		'resource',
		'date'
	];
}
