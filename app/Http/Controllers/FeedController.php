<?php

namespace App\Http\Controllers;

use App\Models\Feed;
use App\Models\Import;
use Illuminate\Http\Request;

class FeedController extends Controller {
    public function index() {

        return view('index');
    }

    public static function importFeed(string $url) {

        // Dont process wrong url format
        if (filter_var($url, FILTER_VALIDATE_URL) === false) {
            return false;
        }

        // Load xml data
        $rss = @simplexml_load_file($url);

        // Dont process wrong xml
        if (!$rss) {
            return false;
        }

        // Load latest import for resource
        $import = Import::where('resource', '=', $url)->orderBy('id', 'desc')->first();
        $lastImport = $import ? $import->date->toDateTimeString() : '';

        foreach ($rss->channel->item as $post) {

            $publictaion = $post->pubDate ? $post->pubDate : date('Y-m-d H:i:s');
            $category = $post->category ? (string) $post->category : 'Uncategorized';

            // Limit feed processing to those which vere published after last import 
            if (strtotime($lastImport) < strtotime($publictaion)) {

                $feed = new Feed();

                $feed->provider = $rss->channel->title;
                $feed->title = $post->title;
                $feed->link = $post->link;
                $feed->category = $category;
                $feed->description = (string) $post->description;
                $feed->publication = $publictaion;

                if (!$feed->save()) {

                    // @TODO Log error function
                }
            }
        }

        $import = new Import();

        $import->resource = $url;
        $import->date = date('Y-m-d H:i:s');

        /*
        @TODO We could store import status, 
        whether there were any errors e.g. $import->errors = count($error)
        Not required per current specs.
        */

        $import->save();
    }

    public function getPosts(string $param = '', string $value = '') {

        if ($param && $value) {
            $feeds = Feed::where($param, '=', $value)->get();
        } else {
            $feeds = Feed::all();
        }

        return response()->json($feeds);
    }
}
