<?php

namespace App\Console;

use App\Http\Controllers\FeedController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {

            // @TODO Create dedicated resources table in DB and pull them dynamically from there
            FeedController::importFeed('https://rss.com/blog/feed/');
            FeedController::importFeed('https://www.feedforall.com/sample-feed.xml');
            
        })->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
