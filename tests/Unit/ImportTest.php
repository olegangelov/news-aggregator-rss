<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Http\Controllers\FeedController;

class ImportTest extends TestCase
{
    /**
     * Wrong url import test.
     *
     * @return void
     */
    public function test_import_wrong_url()
    {
        $this->assertFalse(FeedController::importFeed('wrong url'));
    }

    /**
     * Wrong xml import test.
     *
     * @return void
     */
    public function test_import_wrong_xml()
    {
        $this->assertFalse(FeedController::importFeed('https://google.com'));
    }
}
