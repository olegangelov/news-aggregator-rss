<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ApiTest extends TestCase {
    /**
     * A basic api test.
     *
     * @return void
     */
    public function test_api_returns_a_successful_response() {
        $response = $this->get('/api/get_posts');

        $response->assertStatus(200);
    }

    /**
     * A basic api specific post test.
     *
     * @return void
     */
    public function test_specific_post_api_returns_a_successful_response() {
        $rand = rand(1, 9999);
        $response = $this->get('/api/get_posts/id/' . ceil($rand));

        $response->assertStatus(200);
    }
}
